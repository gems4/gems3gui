@echo off
setlocal EnableDelayedExpansion 

set ROOT_DIR=%cd%
set LOCALINSTALL=C:/usr/local
set PROGFILES=%ProgramFiles%
REM if not "%ProgramFiles(x86)%" == "" set PROGFILES=%ProgramFiles(x86)%

REM Check if Visual Studio 2017 community is installed
set MSVCDIR="%PROGFILES%\Microsoft Visual Studio\2017\Community"
set VCVARSALLPATH="%PROGFILES%\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat"        
if exist %MSVCDIR% (
  if exist %VCVARSALLPATH% (
   	set COMPILER_VER="2017"
        set COMPILER_VER_NAME="Visual Studio 15 2017"
        echo Using Visual Studio 2017 Community
	goto setup_env
  )
)

REM Check if Visual Studio 2019 community is installed
set MSVCDIR="%PROGFILES%\Microsoft Visual Studio\2019\Community"
set VCVARSALLPATH="%PROGFILES%\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat"        
if exist %MSVCDIR% (
  if exist %VCVARSALLPATH% (
   	set COMPILER_VER="2019"
        set COMPILER_VER_NAME="Visual Studio 16 2019"
        echo Using Visual Studio 2019 Community
	goto setup_env
  )
)

REM Check if Visual Studio 2022 community is installed
set MSVCDIR="%PROGFILES%\Microsoft Visual Studio\2022\Community"
set VCVARSALLPATH="%PROGFILES%\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvarsall.bat"        
if exist %MSVCDIR% (
  if exist %VCVARSALLPATH% (
   	set COMPILER_VER="2022"
        set COMPILER_VER_NAME="Visual Studio 17 2022"
        echo Using Visual Studio 2022 Community
	goto setup_env
  )
)


echo No compiler : Microsoft Visual Studio 2017, 2019 or 2022 Community is not installed.
goto end

:setup_env

echo "%MSVCDIR%\VC\Auxiliary\Build\vcvarsall.bat"
call %MSVCDIR%\VC\Auxiliary\Build\vcvarsall.bat x64

mkdir tmp_all
cd tmp_all


echo
echo ******                		 ******
echo ****** Compiling GEMS3K         ******
echo ******                		 ******
echo



echo Get gems3k from git...
git clone https://dmiron@bitbucket.org/gems4/gems3k.git
cd gems3k


rem Set the standard INCLUDE search path
rem
set INCLUDE=%LOCALINSTALL%/include;%INCLUDE%
set LIB=%LOCALINSTALL%/lib;%LIB%
set PATH=%LOCALINSTALL%/bin;%PATH%
echo %INCLUDE%

echo "Configuring..."
cmake -G%COMPILER_VER_NAME% -DUSE_SPDLOG_PRECOMPILED=ON -DBUILD_SHARED_LIBS=OFF -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=%LOCALINSTALL% .. -S . -B build
echo "Building..."
cmake --build build  --config Release --target install
cd ..\..


REM Housekeeping
REM rd /s /q tmp_all

:end

