@echo off
setlocal EnableDelayedExpansion 

set ROOT_DIR=%cd%
set LOCALINSTALL=C:\usr\local
set LOCAL_QT_PATH=C:\Qt\6.4.1\msvc2019_64
set PROGFILES=%ProgramFiles%
REM if not "%ProgramFiles(x86)%" == "" set PROGFILES=%ProgramFiles(x86)%

REM Check if Visual Studio 2017 comunity is installed
set MSVCDIR="%PROGFILES%\Microsoft Visual Studio\2017\Community"
set VCVARSALLPATH="%PROGFILES%\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat"        
if exist %MSVCDIR% (
  if exist %VCVARSALLPATH% (
   	set COMPILER_VER="2017"
        set COMPILER_VER_NAME="Visual Studio 15 2017"
        echo Using Visual Studio 2017 Community
	goto setup_env
  )
)

REM Check if Visual Studio 2019 comunity is installed
set MSVCDIR="%PROGFILES%\Microsoft Visual Studio\2019\Community"
set VCVARSALLPATH="%PROGFILES%\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat"        
if exist %MSVCDIR% (
  if exist %VCVARSALLPATH% (
   	set COMPILER_VER="2019"
        set COMPILER_VER_NAME="Visual Studio 16 2019"
        echo Using Visual Studio 2019 Community
	goto setup_env
  )
)

REM Check if Visual Studio 2022 community is installed
set MSVCDIR="%PROGFILES%\Microsoft Visual Studio\2022\Community"
set VCVARSALLPATH="%PROGFILES%\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvarsall.bat"        
if exist %MSVCDIR% (
  if exist %VCVARSALLPATH% (
   	set COMPILER_VER="2022"
        set COMPILER_VER_NAME="Visual Studio 17 2022"
        echo Using Visual Studio 2022 Community
	goto setup_env
  )
)



echo No compiler : Microsoft Visual Studio 2017, 2019, or 2022 Community is not installed.
goto end

:setup_env


echo "%MSVCDIR%\VC\Auxiliary\Build\vcvarsall.bat"
call %MSVCDIR%\VC\Auxiliary\Build\vcvarsall.bat x64

rem Set the standard INCLUDE search path
rem
rem set INCLUDE=%LOCALINSTALL%\include;%INCLUDE%
rem set LIB=%LOCALINSTALL%\lib;%LIB%
rem set PATH=%LOCALINSTALL%\bin;%PATH%
echo %INCLUDE%


echo "Configuring..."
cmake -G%COMPILER_VER_NAME% -DCMAKE_BUILD_TYPE=Release -DUSE_QT6=ON -DUSE_SPDLOG_PRECOMPILED=ON -DCMAKE_PREFIX_PATH=%LOCAL_QT_PATH% -DCMAKE_INSTALL_PREFIX=%LOCALINSTALL% .. -S . -B build
echo "Building..."
cmake --build build  --config Release

:end

