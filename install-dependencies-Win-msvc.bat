@echo off
setlocal EnableDelayedExpansion 

set ROOT_DIR=%cd%
set LOCALINSTALL=C:/usr/local
set PROGFILES=%ProgramFiles%
REM if not "%ProgramFiles(x86)%" == "" set PROGFILES=%ProgramFiles(x86)%

REM Check if Visual Studio 2017 community is installed
set MSVCDIR="%PROGFILES%\Microsoft Visual Studio\2017\Community"
set VCVARSALLPATH="%PROGFILES%\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat"        
if exist %MSVCDIR% (
  if exist %VCVARSALLPATH% (
   	set COMPILER_VER="2017"
        set COMPILER_VER_NAME="Visual Studio 15 2017"
        echo Using Visual Studio 2017 Community
	goto setup_env
  )
)

REM Check if Visual Studio 2019 community is installed
set MSVCDIR="%PROGFILES%\Microsoft Visual Studio\2019\Community"
set VCVARSALLPATH="%PROGFILES%\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat"        
if exist %MSVCDIR% (
  if exist %VCVARSALLPATH% (
   	set COMPILER_VER="2019"
        set COMPILER_VER_NAME="Visual Studio 16 2019"
        echo Using Visual Studio 2019 Community
	goto setup_env
  )
)

REM Check if Visual Studio 2022 community is installed
set MSVCDIR="%PROGFILES%\Microsoft Visual Studio\2022\Community"
set VCVARSALLPATH="%PROGFILES%\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvarsall.bat"        
if exist %MSVCDIR% (
  if exist %VCVARSALLPATH% (
   	set COMPILER_VER="2022"
        set COMPILER_VER_NAME="Visual Studio 17 2022"
        echo Using Visual Studio 2022 Community
	goto setup_env
  )
)


echo No compiler : Microsoft Visual Studio 2017, 2019 or 2022 Community is not installed.
goto end

:setup_env

echo "%MSVCDIR%\VC\Auxiliary\Build\vcvarsall.bat"
call %MSVCDIR%\VC\Auxiliary\Build\vcvarsall.bat x64

mkdir tmp_all
cd tmp_all

echo
echo ******                		 ******
echo ****** Compiling spdlog      ******
echo ******                		 ******
echo

echo Get spdlog from git...
git clone https://github.com/gabime/spdlog -b v1.10.0
cd spdlog

echo "Configuring..."
REM cmake -G%COMPILER_VER_NAME% -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_INSTALL_PREFIX=%LOCALINSTALL% .. -S . -B build
cmake -G%COMPILER_VER_NAME%  -DCMAKE_INSTALL_PREFIX=%LOCALINSTALL% .. -S . -B build
echo "Building..."
cmake --build build  --config Release --target install
cd ..

echo
echo ******                		 ******
echo ****** Compiling nlohmann      ******
echo ******                		 ******
echo

echo Get nlohmann from git...
git clone https://github.com/nlohmann/json.git
cd json

echo "Configuring..."
cmake -G%COMPILER_VER_NAME% -DJSON_BuildTests=OFF -DCMAKE_INSTALL_PREFIX=%LOCALINSTALL% .. -S . -B build
echo "Building..."
cmake --build build  --target install
cd ..


echo
echo ******                		 ******
echo ****** Compiling Eigen      ******
echo ******                		 ******
echo

echo Get Eigen from git...
git clone https://gitlab.com/libeigen/eigen.git -b 3.4.0
cd eigen

echo "Configuring..."
cmake -G%COMPILER_VER_NAME%  -DCMAKE_INSTALL_PREFIX=%LOCALINSTALL% .. -S . -B build
echo "Building..."
cmake --build build  --target install
cd ..


echo
echo ******                		 ******
echo ****** Compiling ChemicalFun      ******
echo ******                		 ******
echo

echo Get ChemicalFun from git...
git clone https://bitbucket.org/gems4/chemicalfun.git
cd chemicalfun

echo "Configuring..."
cmake -G%COMPILER_VER_NAME% -DCHEMICALFUN_BUILD_PYTHON=OFF -DCMAKE_INSTALL_PREFIX=%LOCALINSTALL% .. -S . -B build
echo "Building..."
cmake --build build  --config Release --target install
cd ..


echo
echo ******                		 ******
echo ****** Compiling ThermoFun         ******
echo ******                		 ******
echo

echo Get ThermoFun from git...
git clone https://github.com/thermohub/thermofun.git
cd thermofun

echo "Configuring..."
cmake -G%COMPILER_VER_NAME%  -DTFUN_BUILD_PYTHON=OFF -DCMAKE_INSTALL_PREFIX=%LOCALINSTALL% .. -S . -B build
echo "Building..."
cmake --build build  --config Release --target install
cd ..\..


REM Housekeeping
rd /s /q tmp_all

:end

