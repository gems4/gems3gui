rem Edit the name of installer file to be generated, Gems3.7.1-xxxxxxx.yyyyyyy-win64-qtinstall.exe
rem by replacing xxxxxxx with commit # of gems3gui and yyyyyyy with commit # of gems3k repositories.
rem Then generate the installer by opening a command-line terminal from this directory and executing the following command:
C:\Qt\Tools\QtInstallerFramework\3.2\bin\binarycreator.exe --offline-only -c config/config.xml -p packages Gems3.7.1-5d15bac.b0d3f02-win64-lr-qtinstall.exe

