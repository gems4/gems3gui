
function Component()
{
    // default constructor
}

Component.prototype.createOperations = function()
{
    // call default implementation to actually install GEMS!
    component.createOperations();

    if (systemInfo.productType === "windows") {
        component.addOperation("CreateShortcut", "@TargetDir@/GEMSFITS/gemsfits.exe", 
	    "@StartMenuDir@/GEMSFITS.lnk", "workingDirectory=@TargetDir@", 
	    "description=Start GEMSFITS");
    }
}
