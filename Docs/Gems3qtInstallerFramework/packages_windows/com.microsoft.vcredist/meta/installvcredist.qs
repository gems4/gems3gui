
function Component()
{
    // default constructor
}

Component.prototype.createOperations = function()
{
    component.createOperations();

    if (systemInfo.productType === "windows") {
        component.addElevatedOperation("Execute", "@TargetDir@/dependsupon/VC_redist.x64.exe", 
	    "workingDirectory=@TargetDir@/dependsupon", 
	    "description=Install Microsoft Visual C/C++ redistributable runtime libraries for Windows 64bit");
    }
}
