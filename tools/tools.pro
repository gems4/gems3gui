TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

DEFINES  += NODEARRAYLEVEL
#DEFINES += USE_NLOHMANNJSON
DEFINES += NDEBUG
#!win32:!macx-clang:DEFINES += OVERFLOW_EXCEPT  #compile with nan inf exceptions

SERVICES4_CPP  =  ../GUI/Services4
DATAMAN_CPP    =  ../GUI/Dataman
GEMS3K_CPP     =  ../../standalone/GEMS3K

SERVICES4_H  =  $$SERVICES4_CPP
DATAMAN_H    =  $$DATAMAN_CPP
GEMS3K_H     =  $$GEMS3K_CPP

DEPENDPATH   += $$SERVICES4_H
DEPENDPATH   += $$DATAMAN_H
DEPENDPATH   += $$GEMS3K_H

INCLUDEPATH   += $$SERVICES4_H
INCLUDEPATH   += $$DATAMAN_H
INCLUDEPATH   += $$GEMS3K_H

OBJECTS_DIR       = obj

HEADERS	 += $$GEMS3K_H/verror.h  \
            $$GEMS3K_H/v_detail.h \
            $$GEMS3K_H/v_service.h \
            $$DATAMAN_H/v_user.h \
            nlohmann/json.h \
            config.h


SOURCES	+=  $$GEMS3K_CPP/v_detail.cpp \
            $$GEMS3K_CPP/v_service.cpp \
            $$DATAMAN_CPP/v_user.cpp \
            config.cpp \
            main.cpp
